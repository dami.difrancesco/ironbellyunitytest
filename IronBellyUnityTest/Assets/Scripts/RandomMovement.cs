﻿using UnityEngine;

public class RandomMovement : MonoBehaviour
{
    [SerializeField] private float xRange = 10f;
    [SerializeField] private float yRange = 5f;
    [SerializeField] private float zRange = 8f;
    [SerializeField] private float velocity = 3f;
    [SerializeField] private float changeDirectionInterval = 5f; // Interval for changing direction

    private Vector3 randomDirection; // Precalculated random vector
    private float timeSinceLastDirectionChange;

    private void Start()
    {
        // Precalculate a random vector within the defined range
        randomDirection = GetRandomDirection();
        timeSinceLastDirectionChange = 0f;

        // Set an initial random position within the defined ranges
        Vector3 initialPosition = new Vector3(
            Random.Range(-xRange, xRange),
            Random.Range(-yRange, yRange),
            Random.Range(-zRange, zRange)
        );
        transform.position = initialPosition;
    }

    private void Update()
    {
        // Move this object in the random direction with a constant velocity
        Vector3 newPosition = transform.position + randomDirection * velocity * Time.deltaTime;

        // Limit the position within the defined ranges
        newPosition.x = Mathf.Clamp(newPosition.x, -xRange, xRange);
        newPosition.y = Mathf.Clamp(newPosition.y, -yRange, yRange);
        newPosition.z = Mathf.Clamp(newPosition.z, -zRange, zRange);

        transform.position = newPosition;

        // Check if it's time to change direction
        timeSinceLastDirectionChange += Time.deltaTime;
        if (timeSinceLastDirectionChange >= changeDirectionInterval)
        {
            randomDirection = GetRandomDirection();
            timeSinceLastDirectionChange = 0f;
        }
    }

    private Vector3 GetRandomDirection()
    {
        return Random.insideUnitSphere.normalized;
    }
}

