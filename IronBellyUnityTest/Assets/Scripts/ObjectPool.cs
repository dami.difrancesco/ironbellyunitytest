using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ObjectPool : MonoBehaviour
{
    [SerializeField] private TMP_InputField inputFiedPoolSize;
    [SerializeField] private GameObject prefab; // The object we want to instantiate
    [SerializeField] private int initialPoolSize = 10; // Initial size of the pool
    private List<GameObject> poolObjects = new List<GameObject>(); // List of objects in the pool

    private void Awake()
    {
        initialPoolSize = Int32.Parse(inputFiedPoolSize.text);
        inputFiedPoolSize.onValueChanged.AddListener(delegate { ValueChangeCheck(); });

        // Create the container for objects in the pool
        poolObjects.Clear();
        for (int i = 0; i < initialPoolSize; i++)
        {
            CreateNewObject();
        }

        SetNeigbourhood();
    }

    private void SetNeigbourhood()
    {
        for (int i = 0; i < poolObjects.Count; i++)
        {
            poolObjects[i].GetComponent<FindNearestNeighbour>().SetMyNeigbourhood(poolObjects);
        }
    }

    private void ValueChangeCheck()
    {
        try
        {
            ResizePool(Int32.Parse(inputFiedPoolSize.text));
        }
        catch (Exception e)
        {
            Debug.LogWarning("Input is empty");
        }
    }

    // Method to get an object from the pool
    public GameObject GetObject()
    {
        foreach (GameObject obj in poolObjects)
        {
            if (!obj.activeSelf)
            {
                obj.SetActive(true);
                return obj;
            }
        }
        // If no inactive objects, create a new one and add it to the pool
        return CreateNewObject();
    }

    // Method to return an object to the pool
    public void ReturnObject(GameObject obj)
    {
        obj.SetActive(false);
    }

    // Create a new object and add it to the pool
    private GameObject CreateNewObject()
    {
        GameObject newObj = Instantiate(prefab, transform);
        poolObjects.Add(newObj);
        return newObj;
    }

    // Method to resize the pool
    public void ResizePool(int newSize)
    {
        // Calculate the difference between the current size and the new size
        int difference = newSize - initialPoolSize;

        if (difference > 0)
        {
            // Increase pool size by creating new objects only if no inactive objects are available
            for (int i = 0; i < difference; i++)
            {
                if (GetObject() == null)
                {
                    CreateNewObject();
                }
            }
        }
        else if (difference < 0)
        {
            // Decrease pool size by deactivating or removing objects
            for (int i = initialPoolSize - 1; i >= newSize; i--)
            {
                ReturnObject(poolObjects[i]);
            }
        }

        initialPoolSize = newSize;
        SetNeigbourhood();
    }
}
