﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FindNearestNeighbour : MonoBehaviour
{
    private List<GameObject> allNeighbours;
    [SerializeField] private LineRenderer myLine;

    public void SetMyNeigbourhood(List<GameObject> myNeighbourhood)
    {
        allNeighbours = new List<GameObject>();
        foreach (GameObject neighbour in myNeighbourhood)
        {
            if (neighbour.activeInHierarchy) allNeighbours.Add(neighbour);
        }
        allNeighbours.Remove(this.gameObject);
    }

    private void Update()
    {
        DrawLine();
    }

    private void DrawLine()
    {
        // Find the nearest object
        Transform nearestObject = FindNearestObject(transform.position);

        if (nearestObject != null && allNeighbours.Count > 0)
        {
            // Draw a line between this object and the nearest one
            Debug.DrawLine(transform.position, nearestObject.position, Color.red);
            myLine.transform.position = transform.position;
            myLine.SetPosition(1, nearestObject.position - this.transform.position);
        }
        else
        {
            // If no neighbours or nearest object, set line position to zero
            myLine.SetPosition(1, Vector3.zero);
        }
    }

    private Transform FindNearestObject(Vector3 position)
    {
        Transform nearest = null;
        float minDistance = float.MaxValue;

        foreach (GameObject obj in allNeighbours)
        {
            float distance = Vector3.Distance(position, obj.transform.position);
            if (distance < minDistance)
            {
                minDistance = distance;
                nearest = obj.transform;
            }
        }

        return nearest;
    }
}
